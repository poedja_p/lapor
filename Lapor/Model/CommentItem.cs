﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RadyaLabs.Model;

namespace Lapor.Model
{
    public class CommentItem : ModelBase
    {
        private string _nid;
        public string nid
        {
            get { return _nid; }
            set { _nid = value; NotifyPropertyChanged("nid"); }
        }

        private string _timestamp;
        public string timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; NotifyPropertyChanged("timestamp"); }
        }

        private string _tanggal;
        public string tanggal
        {
            get { return _tanggal; }
            set { _tanggal = value; NotifyPropertyChanged("tanggal"); }
        }

        private string _user_id;
        public string user_id
        {
            get { return _user_id; }
            set { _user_id = value; NotifyPropertyChanged("user_id"); }
        }

        private string _hash_photo;
        public string hash_photo
        {
            get { return _hash_photo; }
            set { _hash_photo = value; NotifyPropertyChanged("hash_photo"); }
        }

        private string _content;
        public string content
        {
            get { return _content; }
            set { _content = value; NotifyPropertyChanged("content"); }
        }

        private string _coordinate;
        public string coordinate
        {
            get { return _coordinate; }
            set { _coordinate = value; NotifyPropertyChanged("coordinate"); }
        }

        private object _coordinate_lat;
        public object coordinate_lat
        {
            get { return _coordinate_lat; }
            set { _coordinate_lat = value; NotifyPropertyChanged("coordinate_lat"); }
        }

        private object _coordinate_lon;
        public object coordinate_lon
        {
            get { return _coordinate_lon; }
            set { _coordinate_lon = value; NotifyPropertyChanged("coordinate_lon"); }
        }

        private string _address;
        public string address
        {
            get { return _address; }
            set { _address = value; NotifyPropertyChanged("address"); }
        }

        private string _source;
        public string source
        {
            get { return _source; }
            set { _source = value; NotifyPropertyChanged("source"); }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("status"); }
        }

        private string _user_level_id;
        public string user_level_id
        {
            get { return _user_level_id; }
            set { _user_level_id = value; NotifyPropertyChanged("user_level_id"); }
        }

        private string _ulid;
        public string ulid
        {
            get { return _ulid; }
            set { _ulid = value; NotifyPropertyChanged("ulid"); }
        }

        private string _stream_nid;
        public string stream_nid
        {
            get { return _stream_nid; }
            set { _stream_nid = value; NotifyPropertyChanged("stream_nid"); }
        }

        private string _first_name;
        public string first_name
        {
            get { return _first_name; }
            set { _first_name = value; NotifyPropertyChanged("first_name"); }
        }

        private string _last_name;
        public string last_name
        {
            get { return _last_name; }
            set { _last_name = value; NotifyPropertyChanged("last_name"); }
        }

        private string _full_name;
        public string full_name
        {
            get { return (_first_name + _last_name); }
            set { _full_name = value; NotifyPropertyChanged("full_name"); }
        }

        private string _LevelName;
        public string LevelName
        {
            get { return _LevelName; }
            set { _LevelName = value; NotifyPropertyChanged("LevelName"); }
        }

        private double _rate;
        public double rate
        {
            get { return _rate; }
            set { _rate = value; NotifyPropertyChanged("rate"); }
        }


    }
}

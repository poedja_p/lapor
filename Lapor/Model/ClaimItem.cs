﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RadyaLabs.Model;

namespace Lapor.Model
{
    public class ClaimItem : ModelBase
    {

        private string _nid;
        public string nid
        {
            get { return _nid; }
            set { _nid = value; NotifyPropertyChanged("nid"); }
        }

        private string _stream_nid;
        public string stream_nid
        {
            get { return _stream_nid; }
            set { _stream_nid = value; NotifyPropertyChanged("stream_nid"); }
        }

        private string _user_nid;
        public string user_nid
        {
            get { return _user_nid; }
            set { _user_nid = value; NotifyPropertyChanged("user_nid"); }
        }

        private string _user_level_id;
        public string user_level_id
        {
            get { return _user_level_id; }
            set { _user_level_id = value; NotifyPropertyChanged("user_level_id"); }
        }

        private string _instansi_id;
        public string instansi_id
        {
            get { return _instansi_id; }
            set { _instansi_id = value; NotifyPropertyChanged("instansi_id"); }
        }

        private string _claim_text;
        public string claim_text
        {
            get { return _claim_text; }
            set { _claim_text = value; NotifyPropertyChanged("claim_text"); }
        }

        private string _timestamp;
        public string timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; NotifyPropertyChanged("timestamp"); }
        }

        private string _tanggal;
        public string tanggal
        {
            get { return _tanggal; }
            set { _tanggal = value; NotifyPropertyChanged("tanggal"); }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("status"); }
        }

        private string _sanggah;
        public string sanggah
        {
            get { return _sanggah; }
            set { _sanggah = value; NotifyPropertyChanged("sanggah"); }
        }

        private string _username;
        public string username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged("username"); }
        }

        private string _hash_photo;
        public string hash_photo
        {
            get { return _hash_photo; }
            set { _hash_photo = value; NotifyPropertyChanged("hash_photo"); }
        }

        private string _ulid;
        public string ulid
        {
            get { return _ulid; }
            set { _ulid = value; NotifyPropertyChanged("ulid"); }
        }

        private object _ref_filename;
        public object ref_filename
        {
            get { return _ref_filename; }
            set { _ref_filename = value; NotifyPropertyChanged("ref_filename"); }
        }

        private object _ref_filehash;
        public object ref_filehash
        {
            get { return _ref_filehash; }
            set { _ref_filehash = value; NotifyPropertyChanged("ref_filehash"); }
        }

        private string _first_name;
        public string first_name
        {
            get { return _first_name; }
            set { _first_name = value; NotifyPropertyChanged("first_name"); }
        }

        private string _last_name;
        public string last_name
        {
            get { return _last_name; }
            set { _last_name = value; NotifyPropertyChanged("last_name"); }
        }

        private string _full_name;
        public string full_name
        {
            get { return (_first_name + _last_name); }
            set { _full_name = value; NotifyPropertyChanged("full_name"); }
        }

        private string _LevelName;
        public string LevelName
        {
            get { return _LevelName; }
            set { _LevelName = value; NotifyPropertyChanged("LevelName"); }
        }

        private string _logo;
        public string logo
        {
            get { return _logo; }
            set { _logo = value; NotifyPropertyChanged("logo"); }
        }

        private bool _inDomain;
        public bool inDomain
        {
            get { return _inDomain; }
            set { _inDomain = value; NotifyPropertyChanged("inDomain"); }
        }

        private bool _show_name;
        public bool show_name
        {
            get { return _show_name; }
            set { _show_name = value; NotifyPropertyChanged("show_name"); }
        }

        /*
         * IMPLEMENT LATER
        private List<JsonResultLampiranLaporan> _Lampiran;
        public List<JsonResultLampiranLaporan> Lampiran
        {
            get { return _Lampiran; }
            set { _Lampiran = value; NotifyPropertyChanged("Lampiran"); }
        }*/

        private double _rate;
        public double rate
        {
            get { return _rate; }
            set { _rate = value; NotifyPropertyChanged("rate"); }
        }

    }
}

﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lapor.Model
{
    public class LaporEventArgument : EventArgs
    {
       
            public LaporEventArgument()
            {

            }

            public LaporEventArgument(string value)
            {
                mValue = value;
            }

            public LaporEventArgument(string value, object userToken)
            {
                mValue = value;
                mToken = userToken;
            }

            public LaporEventArgument(string value, Exception error)
            {
                mValue = value;
                mError = error;
            }

            public LaporEventArgument(string value, Exception error, object userToken)
            {
                mValue = value;
                mError = error;
                mToken = userToken;
            }


            private string mValue;

            public string Value
            {
                get { return mValue; }
            }

            private Exception mError;

            public Exception Error
            {
                get { return mError; }
                set { mError = value; }
            }

            private object mToken;

            public object UserToken
            {
                get { return mToken; }
                set { mToken = value; }
            }



    
    }
}

﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lapor.Model
{
    public class StreamModel
    {
        private string _user_id;

        public string user_id
        {
            get { return _user_id; }
            set { _user_id = value; }
        }

        private string _content;

        public string content
        {
            get { return _content; }
            set { _content = value; }
        }

        private string _coordinate;

        public string coordinate
        {
            get { return _coordinate; }
            set { _coordinate = value; }
        }

        private string _latitude;

        public string latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        private string _longitude;

        public string longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        private string _photo = "0";

        public string photo
        {
            get { return _photo; }
            set { _photo = value; }
        }

        private Stream _photoA = null;

        public Stream photoA
        {
            get { return _photoA; }
            set { _photoA = value; }
        }

        private string _source = "Windows Phone";

        public string source
        {
            get { return _source; }
        }

        private string _address = "-";

        public string address
        {
            get { return _address; }
        }

        private string _topic_nid;

        public string topic_nid
        {
            get { return _topic_nid; }
            set { _topic_nid = value; }
        }

        private string _reply_nid;

        public string reply_nid
        {
            get { return _reply_nid; }
            set { _reply_nid = value; }
        }

        private int _TypeReport;

        public int TypeReport
        {
            get { return _TypeReport; }
            set { _TypeReport = value; }
        }

        private int _SelectAnonim;

        public int SelectAnonim
        {
            get { return _SelectAnonim; }
            set { _SelectAnonim = value; }
        }

    }
}

﻿using RadyaLabs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lapor.Model
{
    public class ReportAttachment  : ModelBase
    {

        private string _id;
        public string id
        {
            get { return _id; }
            set { _id = value; NotifyPropertyChanged("id"); }
        }

        private string _stream_nid;
        public string stream_nid
        {
            get { return _stream_nid; }
            set { _stream_nid = value; NotifyPropertyChanged("stream_nid"); }
        }

        private string _filename;
        public string filename
        {
            get { return _filename; }
            set { _filename = value; NotifyPropertyChanged("filename"); }
        }

        private string _filehash;
        public string filehash
        {
            get { return _filehash; }
            set { _filehash = value; NotifyPropertyChanged("filehash"); }
        }

        private string _filetype;
        public string filetype
        {
            get { return _filetype; }
            set { _filetype = value; NotifyPropertyChanged("filetype"); }
        }

        private string _fileext;
        public string fileext
        {
            get { return _fileext; }
            set { _fileext = value; NotifyPropertyChanged("fileext"); }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("status"); }
        }

    }
}

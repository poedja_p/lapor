﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lapor.Model
{
    public class JsonResultLogin
    {
        public int verify { get; set; }
        public string user_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int level_id { get; set; }
        public int instansi_id { get; set; }
        public string KeyToken { get; set; }
    }

    public class JsonResultTopic
    {
        public string nid { get; set; }
        public string parent_nid { get; set; }
        public string name { get; set; }
        public string weight { get; set; }
        public string region_nid { get; set; }
        public List<JsonResultTopic> children { get; set; }
    }

    public class JsonResultLaporan
    {
        public string isY { get; set; }
        public string nid { get; set; }
        public string verified { get; set; }
        public string timestamp { get; set; }
        public string last_activity { get; set; }
        public string last_modified { get; set; }
        public string tanggal { get; set; }
        public string report_type { get; set; }
        public string is_anonim { get; set; }
        public string content { get; set; }
        public string subject { get; set; }
        public string tags { get; set; }
        public string photo { get; set; }
        public string topic_nid { get; set; }
        public string topic_name { get; set; }
        public string area_id { get; set; }
        public string topik_nid { get; set; }
        public object photo_hash { get; set; }
        public string coordinate { get; set; }
        public string coordinate_lat { get; set; }
        public string coordinate_lon { get; set; }
        public string isSticky { get; set; }
        public string agree { get; set; }
        public string disagree { get; set; }
        public string comments { get; set; }
        public string source { get; set; }
        public string status { get; set; }
        public string address { get; set; }
        public string disposisi_nid { get; set; }
        public string ombudsman_category_id { get; set; }
        public string tbl_GolonganTopik_id { get; set; }
        public string isRows { get; set; }
        public string user_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string hash_photo { get; set; }
        public string instansi_id { get; set; }
        public string JmlTL { get; set; }
        public List<JsonResultLampiranLaporan> lampiran { get; set; }
    }

    public class JsonResultLampiranLaporan
    {
        public string id { get; set; }
        public string stream_nid { get; set; }
        public string filename { get; set; }
        public string filehash { get; set; }
        public string filetype { get; set; }
        public string fileext { get; set; }
        public string status { get; set; }
    }

    public class JsonResultClaim
    {
        public string nid { get; set; }
        public string stream_nid { get; set; }
        public string user_nid { get; set; }
        public string user_level_id { get; set; }
        public string instansi_id { get; set; }
        public string claim_text { get; set; }
        public string timestamp { get; set; }
        public string tanggal { get; set; }
        public string status { get; set; }
        public string sanggah { get; set; }
        public string username { get; set; }
        public string hash_photo { get; set; }
        public string ulid { get; set; }
        public object ref_filename { get; set; }
        public object ref_filehash { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string LevelName { get; set; }
        public string logo { get; set; }
        public bool inDomain { get; set; }
        public bool show_name { get; set; }
        public List<JsonResultLampiranLaporan> Lampiran { get; set; }
        public double rate { get; set; }
    }

    public class JsonResultComment
    {
        public string nid { get; set; }
        public string timestamp { get; set; }
        public string tanggal { get; set; }
        public string user_id { get; set; }
        public string hash_photo { get; set; }
        public string content { get; set; }
        public string coordinate { get; set; }
        public object coordinate_lat { get; set; }
        public object coordinate_lon { get; set; }
        public string address { get; set; }
        public string source { get; set; }
        public string status { get; set; }
        public string user_level_id { get; set; }
        public string ulid { get; set; }
        public string stream_nid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string LevelName { get; set; }
        public double rate { get; set; }
    }

    public class JSonResultError
    {
        public int status { get; set; }
        public string nid { get; set; }
        public string error { get; set; }
    }

    //definisi kelas yang digunakan untuk parsing dan bukan data binding tempatkan disini
}

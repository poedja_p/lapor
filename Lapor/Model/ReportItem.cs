﻿using RadyaLabs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lapor.Model
{
    public class ReportItem : ModelBase
    {
        private string _isY;
        public string isY
        {
            get { return _isY; }
            set { _isY = value; NotifyPropertyChanged("isY"); }
        }

        private string _nid;
        public string nid
        {
            get { return _nid; }
            set { _nid = value; NotifyPropertyChanged("nid"); }
        }

        private string _verified;
        public string verified
        {
            get { return _verified; }
            set { _verified = value; NotifyPropertyChanged("verified"); }
        }

        private string _timestamp;
        public string timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; NotifyPropertyChanged("timestamp"); }
        }

        private string _last_activity;
        public string last_activity
        {
            get { return _last_activity; }
            set { _last_activity = value; NotifyPropertyChanged("last_activity"); }
        }

        private string _last_modified;
        public string last_modified
        {
            get { return _last_modified; }
            set { _last_modified = value; NotifyPropertyChanged("last_modified"); }
        }

        private string _tanggal;
        public string tanggal
        {
            get { return _tanggal; }
            set { _tanggal = value; NotifyPropertyChanged("tanggal"); }
        }

        private string _report_type;
        public string report_type
        {
            get { return _report_type; }
            set { _report_type = value; NotifyPropertyChanged("report_type"); }
        }

        private string _is_anonim;
        public string is_anonim
        {
            get { return _is_anonim; }
            set { _is_anonim = value; NotifyPropertyChanged("is_anonim"); }
        }

        private string _content;
        public string content
        {
            get { return _content.Replace("<br />",""); }
            set { _content = value; NotifyPropertyChanged("content"); }
        }

        private string _subject;
        public string subject
        {
            get { return _subject; }
            set { _subject = value; NotifyPropertyChanged("subject"); }
        }

        private string _tags;
        public string tags
        {
            get { return _tags; }
            set { _tags = value; NotifyPropertyChanged("tags"); }
        }

        private string _photo;
        public string photo
        {
            get { return _photo; }
            set { _photo = value; NotifyPropertyChanged("photo"); }
        }

        private string _topic_nid;
        public string topic_nid
        {
            get { return _topic_nid; }
            set { _topic_nid = value; NotifyPropertyChanged("topic_nid"); }
        }

        private string _topic_name;
        public string topic_name
        {
            get { return _topic_name; }
            set { _topic_name = value; NotifyPropertyChanged("topic_name"); }
        }

        private string _area_id;
        public string area_id
        {
            get { return _area_id; }
            set { _area_id = value; NotifyPropertyChanged("area_id"); }
        }

        private string _topik_nid;
        public string topik_nid
        {
            get { return _topik_nid; }
            set { _topik_nid = value; NotifyPropertyChanged("topik_nid"); }
        }

        private object _photo_hash;
        public object photo_hash
        {
            get { return _photo_hash; }
            set { _photo_hash = value; NotifyPropertyChanged("photo_hash"); }
        }

        private string _photo_url;
        public string photo_url
        {
            get {

                if (hash_photo != null)
                    _photo_url = "http://lapor.ukp.go.id/images/users/" + hash_photo.ToString() + ".jpg" ;
                return _photo_url; }
            set { _photo_url = value; NotifyPropertyChanged("photo_url"); }
        }


        private string _coordinate;
        public string coordinate
        {
            get { return _coordinate; }
            set { _coordinate = value; NotifyPropertyChanged("coordinate"); }
        }

        private string _coordinate_lat;
        public string coordinate_lat
        {
            get { return _coordinate_lat; }
            set { _coordinate_lat = value; NotifyPropertyChanged("coordinate_lat"); }
        }

        private string _coordinate_lon;
        public string coordinate_lon
        {
            get { return _coordinate_lon; }
            set { _coordinate_lon = value; NotifyPropertyChanged("coordinate_lon"); }
        }

        private string _isSticky;
        public string isSticky
        {
            get { return _isSticky; }
            set { _isSticky = value; NotifyPropertyChanged("isSticky"); }
        }

        private string _agree;
        public string agree
        {
            get { return _agree; }
            set { _agree = value; NotifyPropertyChanged("agree"); }
        }

        private string _disagree;
        public string disagree
        {
            get { return _disagree; }
            set { _disagree = value; NotifyPropertyChanged("disagree"); }
        }

        private string _comments;
        public string comments
        {
            get { return _comments; }
            set { _comments = value; NotifyPropertyChanged("comments"); }
        }

        private string _source;
        public string source
        {
            get { return _source; }
            set { _source = value; NotifyPropertyChanged("source"); }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("status"); }
        }

        private string _address;
        public string address
        {
            get { return _address; }
            set { _address = value; NotifyPropertyChanged("address"); }
        }

        private string _disposisi_nid;
        public string disposisi_nid
        {
            get { return _disposisi_nid; }
            set { _disposisi_nid = value; NotifyPropertyChanged("disposisi_nid"); }
        }

        private string _ombudsman_category_id;
        public string ombudsman_category_id
        {
            get { return _ombudsman_category_id; }
            set { _ombudsman_category_id = value; NotifyPropertyChanged("ombudsman_category_id"); }
        }

        private string _tbl_GolonganTopik_id;
        public string tbl_GolonganTopik_id
        {
            get { return _tbl_GolonganTopik_id; }
            set { _tbl_GolonganTopik_id = value; NotifyPropertyChanged("tbl_GolonganTopik_id"); }
        }

        private string _isRows;
        public string isRows
        {
            get { return _isRows; }
            set { _isRows = value; NotifyPropertyChanged("isRows"); }
        }

        private string _user_id;
        public string user_id
        {
            get { return _user_id; }
            set { _user_id = value; NotifyPropertyChanged("user_id"); }
        }

        private string _first_name;
        public string first_name
        {
            get { return _first_name; }
            set { _first_name = value; NotifyPropertyChanged("first_name"); }
        }

        private string _full_name;
        public string full_name
        {
            get {return (first_name + " " + last_name).ToString().ToUpper(); }
            set { _full_name = value; NotifyPropertyChanged("full_name"); }
        }

        private string _last_name;
        public string last_name
        {
            get { return _last_name; }
            set { _last_name = value; NotifyPropertyChanged("last_name"); }
        }

        private string _hash_photo;
        public string hash_photo
        {
            get { return _hash_photo; }
            set { _hash_photo = value; NotifyPropertyChanged("hash_photo"); }
        }

        private string _instansi_id;
        public string instansi_id
        {
            get { return _instansi_id; }
            set { _instansi_id = value; NotifyPropertyChanged("instansi_id"); }
        }

        private string _JmlTL;
        public string JmlTL
        {
            get { return _JmlTL; }
            set { _JmlTL = value; NotifyPropertyChanged("JmlTL"); }
        }

        private List<ReportAttachment> _lampiran;
        public List<ReportAttachment> lampiran
        {
            get {
                return _lampiran;
            }
            set {
                _lampiran = value;
                NotifyPropertyChanged("lampiran");
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using Lapor.Helper;
using Lapor.Model;
using Lapor.ViewModel;

namespace Lapor.View
{
    public partial class SendCommentPage : PhoneApplicationPage
    {
        private string selectedIndex = "";

        public SendCommentPage()
        {
            InitializeComponent();
        }

        // When page is navigated to set data context to selected item in list
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedIndex))
            {
                int index = int.Parse(selectedIndex);
                NIDTextBox.Text = App.ReportViewModel.Items[index].nid.ToString();
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            ServiceHelper service = new ServiceHelper();
            GeoCoordinateWatcher geoWatcher = new GeoCoordinateWatcher();
            geoWatcher.Start();
            StreamModel streamModel = new StreamModel();
            streamModel.reply_nid = NIDTextBox.Text;
            streamModel.content = ContentTextBox.Text;
            streamModel.latitude = geoWatcher.Position.Location.Latitude.ToString();
            streamModel.longitude = geoWatcher.Position.Location.Longitude.ToString();
            service.SendStreamComment(streamModel);
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(service_DataServiceCompleted);
        }

        void service_DataServiceCompleted(object sender, LaporEventArgument e)
        {
            string s = e.Value;
        }
    }
}
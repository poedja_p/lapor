﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RadyaLabs.Common;
using Lapor.Model;

namespace Lapor.View
{
    public partial class ConfigurationPage : PhoneApplicationPage
    {
        public ConfigurationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (PhoneApplicationService.Current.State.ContainsKey("USE_LOCATION"))
            {
                bool useLocation = (bool)SettingHelper.GetKeyValue<bool>("USE_LOCATION");
                ToolkitSwitch.IsChecked = useLocation;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SettingHelper.SetKeyValue<bool>("USE_LOCATION", ToolkitSwitch.IsChecked.Value);
            
        }

        private void OnLogoutTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.RemoveBackEntry();
            SettingHelper.SetKeyValue<UserModel>("USER_ACCOUNT", null);
            NavigationService.Navigate(new Uri("/View/LoginPage.xaml?logout=true", UriKind.Relative));
        }
    }
}
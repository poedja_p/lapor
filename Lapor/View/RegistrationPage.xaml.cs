﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Lapor.Helper;
using Lapor.Model;
using Microsoft.Phone.Shell;

namespace Lapor.View
{
    public partial class RegistrationPage : PhoneApplicationPage
    {
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };
        
        public RegistrationPage()
        {
            InitializeComponent();
            progressBar.IsVisible = false;
            SystemTray.SetProgressIndicator(this, progressBar);
        }

        public void register(string email, string email2, string password, string password2, string name1, string name2, string sex, string birthday, string phone) {
            if (!email.Equals(email2)) {
                MessageBox.Show("E-mail tidak cocok.");
            } else if (!password.Equals(password2)) {
                MessageBox.Show("Password tidak cocok.");
            } else {
                progressBar.IsVisible = true;
                progressBar.Text = "Memroses pendaftaran...";
                string date = MathHelper.ConvertToEpoch(DateTime.Parse(birthday)).ToString();
                ServiceHelper service = new ServiceHelper();
                service.DataServiceCompleted += new EventHandler<Model.LaporEventArgument>(RegisterComplete);
                service.Register(email, password, name1, name2, sex, date, phone);
            }
        }

        void RegisterComplete(object sender, Model.LaporEventArgument e) {
            progressBar.IsVisible = false;
            progressBar.Text = String.Empty;
            if (e.Error == null) {
                try {
                    JObject err = JObject.Parse(e.Value);
                    MessageBox.Show((string)err["msg"]);
                    if ((int)err["registration"] != 0) {
                        NavigationService.GoBack();
                    }
                } catch (Exception ee) {
                }
            } else {
                MessageBox.Show("Registrasi tidak berhasil.");
            }
        }

        private void OnButtonRegisterClicked(object sender, EventArgs e) {
            string sek = "";
            if (RadioButtonM.IsChecked == true) {
                sek = "0";
            } else if (RadioButtonF.IsChecked == true) {
                sek = "1";
            }
            if (TextBoxEmail.Text == "" || TextBoxEmail2.Text == "") {
                MessageBox.Show("Masukkan alamat e-mail.");
            } else if (PasswordBoxPassword.Password == "" || PasswordBoxPassword2.Password == "") {
                MessageBox.Show("Masukkan password.");
            } else if (CheckAgree.IsChecked == false) {
                MessageBox.Show("Anda harus setuju dengan Syarat & Ketentuan yang ada.");
            } else {
                register(TextBoxEmail.Text, TextBoxEmail2.Text, PasswordBoxPassword.Password, PasswordBoxPassword2.Password, TextBoxName1.Text, TextBoxName2.Text, sek, DatePickerBirthday.ValueString, TextBoxPhone.Text);
            }
        }
    }
}
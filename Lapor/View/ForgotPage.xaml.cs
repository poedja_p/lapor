﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Lapor.Helper;
using Lapor.Model;

namespace Lapor.View {
    public partial class ForgotPage : PhoneApplicationPage {
        public ForgotPage() {
            InitializeComponent();
        }

        void ForgotComplete(object sender, Model.LaporEventArgument e) {
            if (e.Error == null) {
                MessageBox.Show("Konfirmasi lupa password telah dikirim. Silakan cek e-mail Anda.");
                NavigationService.GoBack();
            } else {
            }
        }

        private void OnButtonProceedClicked(object sender, EventArgs e) {
            if (TextBoxEmail.Text != "") {
                ServiceHelper service = new ServiceHelper();
                service.DataServiceCompleted += new EventHandler<Model.LaporEventArgument>(ForgotComplete);
                service.Forgot(TextBoxEmail.Text);
            } else {
                MessageBox.Show("Masukkan email.");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using Microsoft.Phone.Shell;
using Lapor.Model;
using Lapor.ViewModel;
using Lapor.Helper;

namespace Lapor.View
{
    public partial class DetailReportPage : PhoneApplicationPage
    {
        DetailReportViewModel vm;
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };

        public DetailReportPage()
        {
            vm = new DetailReportViewModel();
            DataContext = vm;
            vm.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(vm_PropertyChanged);
            InitializeComponent();
            SystemTray.SetProgressIndicator(this, progressBar);
        }

        // When page is navigated to set data context to selected item in list
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            vm.SelectedReport = PhoneApplicationService.Current.State["SELECTED_REPORT"] as ReportItem;
            vm.Load();
        }

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy")
            {
                if (vm.IsBusy)
                {
                    progressBar.IsVisible = true;
                    if (vm.isStatusDataMenunggu)
                        progressBar.Text = MessageHelper.FETCH;
                    else progressBar.Text = MessageHelper.SEND_DATA;
                }
                else
                {
                    progressBar.IsVisible = false;
                    progressBar.Text = String.Empty;
                    this.Focus();

                }
            }
        }

        private void OnCommentKeydown(object sender, KeyEventArgs e)
        {
            //Fungsi AddComment
            if (e.Key == Key.Enter)
            {
                vm.SendComment(CommentTextBox.Text);
                this.Focus();
                CommentTextBox.Text = "";
            }
        }

        private void OnShareClicked(object sender, EventArgs e)
        {
           
            ShareLinkTask task = new ShareLinkTask();
            task.LinkUri = new Uri("http://lapor.ukp.go.id/id/" + vm.SelectedReport.nid);
            task.Message = vm.SelectedReport.subject + " via @LAPOR_UKP4";
            task.Show();
        }

        private void OnDukungClicked(object sender, EventArgs e)
        {
            vm.Vote();
        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Lapor.View
{
    public partial class KategoriPage : PhoneApplicationPage
    {
        public KategoriPage()
        {
            InitializeComponent();
            if (!App.TopicViewModel.IsDataLoaded) App.TopicViewModel.LoadData();
            DataContext = App.TopicViewModel;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (!App.TopicViewModel.IsDataLoaded) App.TopicViewModel.LoadData();
        }


        private void OnKategoriSeletionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lib = (sender as ListBox);
            if (lib.SelectedIndex == -1)
                return;

            //CASTING AS TOPIC
            PhoneApplicationService.Current.State["SELECTED_TOPIC"] = lib.SelectedItem;

            NavigationService.GoBack();

            lib.SelectedIndex = -1;
        }
    }
}
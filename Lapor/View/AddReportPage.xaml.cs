﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;
using Microsoft.Xna.Framework.Media;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Common;
using Lapor.ViewModel;

namespace Lapor.View
{
    public partial class AddReportPage : PhoneApplicationPage
    {
        PhotoChooserTask task;
        Stream PicStream;
        ServiceHelper dataService;
        TopicItemViewModel tempTopic;
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };

        public AddReportPage()
        {
            InitializeComponent();
            task = new PhotoChooserTask();
            task.Completed += task_Completed;
            dataService = new ServiceHelper();
            SystemTray.SetProgressIndicator(this, progressBar);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (PhoneApplicationService.Current.State.ContainsKey("SELECTED_TOPIC"))
            {
                tempTopic = (PhoneApplicationService.Current.State["SELECTED_TOPIC"] as TopicItemViewModel);
                TopicName.Text = tempTopic.Topik.ToUpper();
                PhoneApplicationService.Current.State.Remove("SELECTED_TOPIC");
            }
            else {
                tempTopic = new TopicItemViewModel();
                tempTopic.NID = "-1";
            }
            
        }

        void task_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                PicStream = e.ChosenPhoto;

                MemoryStream ms = new MemoryStream();
                byte[] readBuffer = new byte[4096];
                int bytesRead = -1;
                // Copy the image to isolated storage. 
                while ((bytesRead = e.ChosenPhoto.Read(readBuffer, 0, readBuffer.Length)) > 0)
                {
                    ms.Write(readBuffer, 0, bytesRead);
                }
                PicStream.Seek(0, SeekOrigin.Begin);

                BitmapImage bmp = new BitmapImage();
                bmp.SetSource(ms);
                ImageLampiran.Source = bmp;

                StackPanelLampiran.Visibility = System.Windows.Visibility.Visible;

                WriteableBitmap wb = new WriteableBitmap(bmp);
                WriteableBitmap resized;
                if (wb.PixelWidth > wb.PixelHeight)
                    resized = wb.Resize(800, 600, WriteableBitmapExtensions.Interpolation.Bilinear);
                else
                    resized = wb.Resize(600, 800, WriteableBitmapExtensions.Interpolation.Bilinear);

                var fileStream = new MemoryStream();
                resized.SaveJpeg(fileStream, resized.PixelWidth, resized.PixelHeight, 0, 70);
                fileStream.Seek(0, SeekOrigin.Begin);
                PicStream = fileStream;

                
            }
        }

        private bool Validate() {
            if (tempTopic == null)
            {
                tempTopic = new TopicItemViewModel();
                tempTopic.NID = "-1";
            }
 
            if (TextBoxContent.Text.Length < 10)
            {
                MessageBox.Show("Minimal laporan 10 karakter");
                return false;
            }
            return true;
        }

        private void OnSimpanClicked(object sender, EventArgs e)
        {
            if (Validate())
            {
                progressBar.IsVisible = true;
                progressBar.Text = MessageHelper.SEND_DATA;
                StreamModel report = new StreamModel();
                report.content = TextBoxContent.Text;
               // TextBoxContent.Text = string.Empty;

                Point point = SettingHelper.GetKeyValue<Point>("DefaultLocation");
                report.latitude = point.X.ToString();
                report.longitude = point.Y.ToString();

                if (tempTopic != null)
                    report.topic_nid = tempTopic.NID;
                if (PicStream != null)
                {
                    report.photoA = PicStream;
                    report.photo = "--PHOTO--";
                }

                if (CheckBoxAnonim.IsChecked.Value) {
                    report.SelectAnonim = 1;
                //    CheckBoxAnonim.IsChecked = false;
                }

                if (CheckBoxRahasia.IsChecked.Value) {
                    report.TypeReport = 2;
                  //  CheckBoxRahasia.IsChecked = false;
                }
                dataService.DataServiceCompleted += new EventHandler<LaporEventArgument>(addReport_Completed);
                dataService.SendStream(report);
                // report.
            }
        }

        void addReport_Completed(object sender, LaporEventArgument e)
        {
            Dispatcher.BeginInvoke(() =>
            {
            // Masuk Error
            if (e.Error == null) {
                JSonResultError result = JsonConvert.DeserializeObject<JSonResultError>(e.Value);
                if (result.status == 0)
                {
                    TextBoxContent.Text = string.Empty;
                    CheckBoxAnonim.IsChecked = false;
                     CheckBoxRahasia.IsChecked = false;
                    
                    MessageBox.Show("Terima kasih atas laporan Anda. Laporan Anda sekarang berada pada antrian verifikasi laporan yang akan dilakukan oleh administrator. Kami akan memberikan Anda email notifikasi apabila laporan Anda sudah kami disposisikan ke Kementrian/Lembaga terkait.");
                    NavigationService.GoBack();
                }
                else {
                    MessageBox.Show(result.error);
                }
            }
            //different thread
            
                progressBar.IsVisible = false;
                progressBar.Text = String.Empty;
                this.Focus();
            });
        }

        private void OnAmbilPhotoClicked(object sender, EventArgs e)
        {
           
            task.ShowCamera = true;
            task.Show();
            
        }

        private void OnDeleteClicked(object sender, RoutedEventArgs e)
        {
            PicStream = null;
            ImageLampiran.Source = null;
        }

        private void OnTopicPhotoClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/KategoriPage.xaml", UriKind.Relative));
        }
    }
}
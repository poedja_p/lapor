﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using Lapor.Helper;
using Lapor.Model;
using Lapor.ViewModel;
using Microsoft.Phone.Shell;

namespace Lapor.View
{
    public partial class HotReportPage : PhoneApplicationPage
    {
        MainReportViewModel vm = new MainReportViewModel();
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };
        public HotReportPage()
        {
            InitializeComponent();
            DataContext = vm;
            vm.HotReports.PropertyChanged += vm_PropertyChanged;
            vm.AllReports.PropertyChanged += vm_PropertyChanged;
            vm.MyReports.PropertyChanged += vm_PropertyChanged;
            SystemTray.SetProgressIndicator(this, progressBar);
          
            this.Loaded += HotReportPage_Loaded;
        }

        void HotReportPage_Loaded(object sender, RoutedEventArgs e)
        {
            //AllReportListBox.Button_LoadMore.Click += Button_LoadMore_Click;
            //MyReportListBox.Button_LoadMore.Click += Button_LoadMore_Click;
        }

        void ButtonMyReport_LoadMore_Click(object sender, RoutedEventArgs e)
        {
            vm.MyReports.LoadMore();
        }

        void ButtonAllReport_LoadMore_Click(object sender, RoutedEventArgs e)
        {
            vm.AllReports.LoadMore();
        }

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy")
            {
                if (vm.AllReports.IsBusy || vm.MyReports.IsBusy || vm.HotReports.IsBusy)
                {
                    progressBar.IsVisible = true;
                    progressBar.Text = MessageHelper.FETCH;
                }
                else
                {
                    progressBar.IsVisible = false;
                    progressBar.Text = String.Empty;
                    this.Focus();
                }
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.ContainsKey("login"))
            {
                NavigationService.RemoveBackEntry();
            }
            
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
            {
                if (PhoneApplicationService.Current.State.ContainsKey("SELECTED_TOPIC"))
                {
                    vm.CurrentTopic = PhoneApplicationService.Current.State["SELECTED_TOPIC"] as TopicItemViewModel;

                    //LOAD ULANG ALL REPORT DAN MY REPORT BASED ON TOPIC YG DIPILIH
                    vm.Load();
                    PhoneApplicationService.Current.State.Remove("SELECTED_TOPIC");
                }
            }
            else {
                vm.Load();
            }
        }

        private void ReportsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If selected index is -1 (no selection) do nothing
            ListBox lb = (sender as ListBox);
            if (lb.SelectedIndex == -1)
                return;

            // Navigate to the new page
            PhoneApplicationService.Current.State["SELECTED_REPORT"] = lb.SelectedItem as ReportItem;
            NavigationService.Navigate(new Uri("/View/DetailReportPage.xaml", UriKind.Relative));//?selectedItem=" + lb.SelectedIndex, UriKind.Relative));

            // Reset selected index to -1 (no selection)
            lb.SelectedIndex = -1;
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/SearchResultPage.xaml",UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/KategoriPage.xaml", UriKind.Relative));

            //ServiceHelper service = new ServiceHelper();
            //StreamModel lapor = new StreamModel();
            //lapor.topic_nid = "8"; // infrastructur
            //lapor.content = "Tolong kepada pemerintah Bandung untuk segera memperbaiki jalan Tubagus Ismail. Karena sudah sangat parah. Sering motor terjatuh disana. Apalagi jalan sangat curam. Terima Kasih";
            //GeoCoordinateWatcher geoWatcher = new GeoCoordinateWatcher();
            //geoWatcher.Start();
            //lapor.latitude = geoWatcher.Position.Location.Latitude.ToString();
        }


        private void OnTentangClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/SettingPage.xaml", UriKind.Relative));
        }

        private void OnProfileClicked(object sender, EventArgs e) {
            NavigationService.Navigate(new Uri("/View/EditProfilePage.xaml", UriKind.Relative));
        }

        private void OnPengaturanClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/ConfigurationPage.xaml", UriKind.Relative));
        }

        private void OnAddReportClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/AddReportPage.xaml", UriKind.Relative));
        }

        private void OnListBoxAllReportLoaded(object sender, RoutedEventArgs e)
        {
            AllReportListBox.Button_LoadMore.Click += ButtonAllReport_LoadMore_Click;
        }

        private void OnListBoxMyReportLoaded(object sender, RoutedEventArgs e)
        {
            MyReportListBox.Button_LoadMore.Click += ButtonMyReport_LoadMore_Click;
        }
    }
}
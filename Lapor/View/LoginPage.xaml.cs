﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Common;
using Microsoft.Phone.Shell;

namespace Lapor.View
{
    public partial class LoginPage : PhoneApplicationPage
    {
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };
        ServiceHelper service = new ServiceHelper();
          
        public LoginPage()
        {
            InitializeComponent();
            progressBar.IsVisible = false;
            SystemTray.SetProgressIndicator(this, progressBar);
            service.DataServiceCompleted += new EventHandler<Model.LaporEventArgument>(LoginComplete);
        }


        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.ContainsKey("logout"))
            {
                NavigationService.RemoveBackEntry();
                //NavigationService.GoBack();
            }
        }
     
        public void Login(String email, String password)
        {
            progressBar.IsVisible = true;
            progressBar.Text = MessageHelper.LOADING;
            service.Login(email, password);
        }

        void LoginComplete(object sender, Model.LaporEventArgument e)
        {
            progressBar.IsVisible = false;
            progressBar.Text = String.Empty;
            if (e.Error == null) {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                JsonResultLogin obj = JsonConvert.DeserializeObject<JsonResultLogin>(e.Value);
                UserModel userData = new UserModel()
                {
                    UserId = obj.user_id,
                    FirstName = obj.firstname,
                    KeyToken = obj.KeyToken,
                    LastName = obj.lastname
                };
                
                SettingHelper.SetKeyValue<UserModel>("USER_ACCOUNT", userData);
                NavigationService.Navigate(new Uri("/View/HotReportPage.xaml?login=true", UriKind.Relative));
            } else {
                MessageBox.Show("Login tidak berhasil.");
            }
        }

        private void OnButtonRegisterClicked(object sender, RoutedEventArgs e) {
            NavigationService.Navigate(new Uri("/View/RegistrationPage.xaml", UriKind.Relative));
        }

        private void OnButtonForgotClicked(object sender, RoutedEventArgs e) {
            NavigationService.Navigate(new Uri("/View/ForgotPage.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e) {
         //   NavigationService.RemoveBackEntry();
        }

        private void OnLoginClicked(object sender, EventArgs e)
        {
            if (TextBoxEmail.Text == "")
            {
                MessageBox.Show("Masukkan alamat e-mail.");
            }
            else if (PasswordBoxPassword.Password == "")
            {
                MessageBox.Show("Masukkan password.");
            }
            else
            {
                Login(TextBoxEmail.Text, PasswordBoxPassword.Password);
            }
        }
    }
}
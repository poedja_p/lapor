﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Common;
using Microsoft.Phone.Shell;

namespace Lapor.View {
    public partial class EditProfilePage : PhoneApplicationPage {
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };
        
        public EditProfilePage() {
            InitializeComponent();
            progressBar.IsVisible = false;
            SystemTray.SetProgressIndicator(this, progressBar);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e) {
            TextBoxName1.Text = "";
            TextBoxName2.Text = "";
            TextBoxPhone.Text = "";
            TextBoxBirthday.Text = "";
            RadioButtonM.IsChecked = false;
            RadioButtonF.IsChecked = false;
            ApplicationBar.IsVisible = false;
            progressBar.IsVisible = true;
            progressBar.Text = MessageHelper.FETCH;
            ServiceHelper service = new ServiceHelper();
            service.DataServiceCompleted += new EventHandler<Model.LaporEventArgument>(ProfileLoadComplete);
            service.GetProfile();
        }

        void ProfileLoadComplete(object sender, Model.LaporEventArgument e) {
            ApplicationBar.IsVisible = true;
            progressBar.IsVisible = false;
            progressBar.Text = String.Empty;
            if (e.Error == null) {
                JObject obj = JObject.Parse(e.Value);
                TextBoxName1.Text = (string)obj["first_name"];
                TextBoxName2.Text = (string)obj["last_name"];
                TextBoxPhone.Text = (string)obj["phone"];
                if ((string)obj["sex"] == "0") {
                    RadioButtonM.IsChecked = true;
                } else if ((string)obj["sex"] == "1") {
                    RadioButtonF.IsChecked = true;
                }
                TextBoxBirthday.Text = MathHelper.ConvertFromEpoch(long.Parse(obj["birthday"].ToString())).ToString("yyyy/MM/dd");
                TextBoxId.Text = (string)obj["no_id"];
                //MessageBox.Show(e.Value);
            } else {
            }
        }

        void ProfileComplete(object sender, Model.LaporEventArgument e) {
            progressBar.IsVisible = false;
            progressBar.Text = String.Empty;
            MessageBox.Show("Profil Anda sudah diupdate.");
            if (e.Error == null) {
                JObject obj = JObject.Parse(e.Value);
                //MessageBox.Show(e.Value);
            } else {
            }
        }

        private void OnButtonSubmitClicked(object sender, EventArgs e) {
            string sek = "";
            if (RadioButtonM.IsChecked == true) {
                sek = "0";
            } else if (RadioButtonF.IsChecked == true) {
                sek = "1";
            }
            progressBar.IsVisible = true;
            progressBar.Text = MessageHelper.LOADING;
            //string date = MathHelper.ConvertToEpoch(DateTime.Parse(TextBoxBirthday.Text)).ToString();
            ServiceHelper service = new ServiceHelper();
            service.DataServiceCompleted += new EventHandler<Model.LaporEventArgument>(ProfileComplete);
            service.EditProfile(TextBoxName1.Text, TextBoxName2.Text, TextBoxPhone.Text, sek, TextBoxBirthday.Text, TextBoxId.Text, "0");
        }
    }
}

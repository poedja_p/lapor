﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Lapor.ViewModel;
using Microsoft.Phone.Shell;
using Lapor.Helper;

namespace Lapor.View
{
    public partial class SearchResultPage : PhoneApplicationPage
    {
        ReportViewModel vm;
        ProgressIndicator progressBar = new ProgressIndicator() { IsIndeterminate = true };
        public SearchResultPage()
        {
            InitializeComponent();
            vm = new ReportViewModel();
            DataContext = vm;
            vm.PropertyChanged += vm_PropertyChanged;
            SystemTray.SetProgressIndicator(this, progressBar);
        }

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy")
            {
                if (vm.IsBusy)
                {
                    progressBar.IsVisible = true;
                    progressBar.Text = MessageHelper.SEARCHING;
                }
                else {
                    progressBar.IsVisible = false;
                    progressBar.Text = String.Empty;
                    this.Focus();
                
                }
            }
        }

       

        private void OnSearchKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            { 
                //search
                vm.LoadSearchData((sender as PhoneTextBox).Text);
            }
        }
        

        private void SearchRepostListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             //If selected index is -1 (no selection) do nothing
            if (SearchRepostListBox.SelectedIndex == -1)
                return;

            //Navigate to the new page
            NavigationService.Navigate(new Uri("/View/DetailReportPage.xaml?selectedItem=" + SearchRepostListBox.SelectedIndex, UriKind.Relative));

             //Reset selected index to -1 (no selection)
            SearchRepostListBox.SelectedIndex = -1;
        }

       
    }
}
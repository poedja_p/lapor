﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lapor.ViewModel
{
    public class ReportItemViewModel : INotifyPropertyChanged
    {
        private string _NID;
        public string NID {
            get { return _NID; }
            set { _NID = value; }
        }

        private string _Subject;
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; NotifyPropertyChanged("Subject"); }
        }

        private string _Pelapor;
        public string Pelapor
        {
            get { return _Pelapor; }
            set { _Pelapor = value; NotifyPropertyChanged("Source"); }
        }

        private string _Content;
        public string Content
        {
            get { return _Content; }
            set { _Content = value; NotifyPropertyChanged("Content"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using RadyaLabs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Lapor.ViewModel
{
    public class MainReportViewModel : ViewModelBase
    {
        private ReportViewModel _HotReports;
        public ReportViewModel HotReports
        {
            get {
                return _HotReports;
            }
            set {
                _HotReports = value;
                NotifyPropertyChanged("HotReports");
            }
        }

        private TopicItemViewModel _CurrentTopic;
        public TopicItemViewModel CurrentTopic
        {
            get
            {
                return _CurrentTopic;
            }
            set
            {
                _CurrentTopic = value;
                NotifyPropertyChanged("CurrentTopic");
            }
        }

        private Visibility _CurrentTopicVisibility;
        public Visibility CurrentTopicVisibility
        {
            get
            {
                return _CurrentTopicVisibility;
            }
            set
            {
                _CurrentTopicVisibility = value;
                NotifyPropertyChanged("CurrentTopicVisibility");
            }
        }

        private ReportViewModel _AllReports;
        public ReportViewModel AllReports
        {
            get
            {
                return _AllReports;
            }
            set
            {
                _AllReports = value;
                NotifyPropertyChanged("AllReports");
            }
        }

        private ReportViewModel _MyReports;
        public ReportViewModel MyReports
        {
            get
            {
                return _MyReports;
            }
            set
            {
                _MyReports = value;
                NotifyPropertyChanged("MyReports");
            }
        }

        public MainReportViewModel()
        {
            AllReports = new ReportViewModel(0);
            HotReports = new ReportViewModel(1);
            MyReports = new ReportViewModel(2);
            CurrentTopic = new TopicItemViewModel();
            CurrentTopic.NID = "-1";
            CurrentTopic.Topik = "Semua Topik";
        }

        public void Load()
        {
            if (CurrentTopic == null || CurrentTopic.NID.Equals("-1"))
            {
                HotReports.LoadData();
                AllReports.LoadData();
                MyReports.LoadData();
                CurrentTopicVisibility = Visibility.Collapsed;
            }
            else
            {
                Load(CurrentTopic.NID);
                CurrentTopicVisibility = Visibility.Visible;
            }
        }

        private void Load(string topicNid) {
            HotReports.LoadData(topicNid);
            MyReports.LoadData(topicNid);
            AllReports.LoadData(topicNid);
        }

    }
}

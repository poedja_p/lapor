﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Model;
using RadyaLabs.Common;

namespace Lapor.ViewModel
{
    public class ReportViewModel : ViewModelBase
    {
        ServiceHelper service = new ServiceHelper();
        private int ReportType;
        private bool isLoadMore;
        private bool isRefresh;

        public ReportViewModel()
        {
            this.Items = new ObservableCollection<ReportItem>();
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(ReportCompleted);
            LoadMoreVisibility = System.Windows.Visibility.Collapsed;
            Message = "Tidak ada laporan";
        }

        public ReportViewModel(int reportType=0)
        {
            ReportType = reportType;
            this.Items = new ObservableCollection<ReportItem>();
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(ReportCompleted);
            LoadMoreVisibility = System.Windows.Visibility.Collapsed;
            Message = "Tidak ada laporan";
        }

        //notes to developer : report item view model changes to reportitem
        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>

        private ObservableCollection<ReportItem> _Items;

        public ObservableCollection<ReportItem> Items
        {
            get {
                return _Items;
            }
            set {
                _Items = value;
                NotifyPropertyChanged("Items");
            }
        }


        public bool IsDataLoaded
        {
            get;
            private set;
        }

        private System.Windows.Visibility  _LoadMoreVisibility;
        public System.Windows.Visibility  LoadMoreVisibility
        {
            get
            {
               
                return _LoadMoreVisibility;
            }
            set
            {
                _LoadMoreVisibility = value;
                NotifyPropertyChanged("LoadMoreVisibility");
            }
        }

        private System.Windows.Visibility _MessageVisibility;
        public System.Windows.Visibility MessageVisibility
        {
            get
            {

                return _MessageVisibility;
            }
            set
            {
                _MessageVisibility = value;
                NotifyPropertyChanged("MessageVisibility");
            }
        }

        private string _Message;
        public string Message
        {
            get
            {

                return _Message;
            }
            set
            {
                _Message = value;
                NotifyPropertyChanged("Message");
            }
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// type = 0 , all reports
        /// type = 1, hot topics
        /// type = 2, laporan saya
        /// </summary>
        private void LoadData(int type=0,string topicNid = "-1")
        {
            switch(type)
            {
                case 0:
                    if (topicNid.Equals("-1")) service.GetAllReports();
                    else service.GetStreamsByTopic(topicNid);
                    break;
                case 1:
                    service.GetHotTopics();
                    break;
                case 2 :
                    if (topicNid.Equals("-1")) service.GetStreamsByUsers(-1);
                    else service.GetStreamsUserByCategory(topicNid);
                    break;
                default :
                    service.GetAllReports();
                    break;
            }
         
        }

        public void LoadData(string topicNid="-1")
        {
            isLoadMore = false;
            IsBusy = true;
            LoadMoreVisibility = System.Windows.Visibility.Collapsed;
            LoadData(ReportType,topicNid);
        }

        public void RefreshData(string topicNid = "-1") {
            isRefresh = false;
            IsBusy = true;
            LoadMoreVisibility = System.Windows.Visibility.Collapsed;
            LoadData(ReportType, topicNid);
        }

        public void LoadSearchData(string searchQuery) {

            IsBusy = true;

            UserModel userData = SettingHelper.GetKeyValue<UserModel>("USER_ACCOUNT");
            service.Search(searchQuery);
        }

        void ReportCompleted(object sender, LaporEventArgument e)
        {
            IsBusy = false;
            if (e.Value != null)
            {
                List<ReportItem> result = JsonConvert.DeserializeObject<List<ReportItem>>(e.Value);
                if (result.Count < 10)
                {
                    this.LoadMoreVisibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    this.LoadMoreVisibility = System.Windows.Visibility.Visible;
                }
                if (isLoadMore)
                {
                    foreach (ReportItem item in result)
                    {
                        Items.Add(item);
                    }
                }
                else if (isRefresh) {
                    int counterReport = 0;
                    foreach (ReportItem item in result)
                    {
                        if (!item.nid.Equals(Items[0].nid))
                        {
                            Items.Insert(counterReport++, item);
                        }
                        else {
                            break;
                        }
                    }
                }
                else
                {
                    Items = new ObservableCollection<ReportItem>(result);
                }
                this.IsDataLoaded = true;
                MessageVisibility = Visibility.Collapsed;
            }
            else {
                Items = new ObservableCollection<ReportItem>();
                this.LoadMoreVisibility = System.Windows.Visibility.Collapsed;
                MessageVisibility = Visibility.Visible;
            }
        }

        private void LoadMore(int type=0,string topicNid="-1") {
            switch (type)
            {
                case 0:
                    if (topicNid.Equals("-1")) service.GetAllReports(Items[Items.Count-1].last_activity);
                    else service.GetStreamsByTopic(topicNid, Items[Items.Count - 1].last_activity);
                    break;
                case 1:
                    // Not Supported
                    break;
                case 2:
                    if (topicNid.Equals("-1")) service.GetStreamsByUsers(-1,Items[Items.Count-1].last_activity);
                    else service.GetStreamsUserByCategory(topicNid, Items[Items.Count - 1].last_activity);
                    break;
                default:
                    service.GetAllReports();
                    break;
            }
        }

        public void LoadMore(string topicNid = "-1")
        {
            isLoadMore = true;
            IsBusy = true;
            LoadMoreVisibility = System.Windows.Visibility.Collapsed;
            LoadMore(ReportType,topicNid);
        }
    }
}

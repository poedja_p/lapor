﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Model;

namespace Lapor.ViewModel
{
    public class TopicItemViewModel : ModelBase
    {
        private string _NID;
        public string NID
        {
            get { return _NID; }
            set { _NID = value; }
        }

        private string _Topik;
        public string Topik
        {
            get { return _Topik; }
            set { _Topik = value; NotifyPropertyChanged("Topik"); }
        }

      
    }

    public class TopicViewModel : ViewModelBase
    {
        public TopicViewModel()
        {
            this.Items = new ObservableCollection<TopicItemViewModel>();
            this.IsDataLoaded = false;
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<TopicItemViewModel> Items { get; private set; }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.Items.Clear();
            ServiceHelper service = new ServiceHelper();
            service.GetTopics();
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(LoadTopicCompleted);
        }

        void LoadTopicCompleted(object sender, LaporEventArgument e)
        {
            if (e.Error == null)
            {
                List<JsonResultTopic> result = JsonConvert.DeserializeObject<List<JsonResultTopic>>(e.Value);

                // Base (load ALL)
                TopicItemViewModel item = new TopicItemViewModel();
                item.NID = "-1";
                item.Topik = "Semua Topik";
                this.Items.Add(item);

                // Recursive Parse
                ParseTopic(result);
                this.IsDataLoaded = true;
            }
        }

        private void ParseTopic(List<JsonResultTopic> result)
        {
            foreach (JsonResultTopic laporan in result) {
                TopicItemViewModel item = new TopicItemViewModel();
                item.NID = laporan.nid;
                item.Topik = laporan.name;
                this.Items.Add(item);
                if (laporan.children != null) {
                    ParseTopic(laporan.children);
                }
            }
        }

       
    }
}

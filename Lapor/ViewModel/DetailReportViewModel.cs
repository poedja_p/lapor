﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Lapor.Helper;
using Lapor.Model;
using RadyaLabs.Model;
using RadyaLabs.Common;

namespace Lapor.ViewModel
{
    public class DetailReportViewModel : ViewModelBase
    {
        ServiceHelper serviceComment = new ServiceHelper();
        ServiceHelper serviceClaim = new ServiceHelper();

        private bool _AlreadyVote;
        public bool AlreadyVote
        {
            get { return _AlreadyVote; }
            set { _AlreadyVote = value; NotifyPropertyChanged("AlreadyVote"); }
        }

        private string _CommentMessage;
        public string CommentMessage
        {
            get { return _CommentMessage; }
            set { _CommentMessage = value; NotifyPropertyChanged("CommentMessage"); }
        }

        private Visibility _CommentVisibility;
        public Visibility CommentVisibility
        {
            get { return _CommentVisibility; }
            set { _CommentVisibility = value; NotifyPropertyChanged("CommentVisibility"); }
        }

        private string _TLMessage;
        public string TLMessage
        {
            get { return _TLMessage; }
            set { _TLMessage = value; NotifyPropertyChanged("TLMessage"); }
        }

        private Visibility _TLVisibility;
        public Visibility TLVisibility
        {
            get { return _TLVisibility; }
            set { _TLVisibility = value; NotifyPropertyChanged("TLVisibility"); }
        }

        public bool isStatusDataMenunggu { get; set; }

        public DetailReportViewModel()
        {
            AlreadyVote = false;
            TLMessage = String.Empty;
            TLVisibility = Visibility.Collapsed;
            CommentMessage = String.Empty;
            ClaimItems = new ObservableCollection<ClaimItem>();
            CommentItems = new ObservableCollection<CommentItem>();
            CommentVisibility = Visibility.Collapsed;
            this.CommentItems = new ObservableCollection<CommentItem>();
            serviceComment.DataServiceCompleted += new EventHandler<LaporEventArgument>(CommentCompleted);
            serviceClaim.DataServiceCompleted += new EventHandler<LaporEventArgument>(ClaimCompleted);
        }

        private ObservableCollection<CommentItem> _CommentItems;
        public ObservableCollection<CommentItem> CommentItems
        {
            get
            {
                return _CommentItems;
            }
            set
            {
                _CommentItems = value;
                NotifyPropertyChanged("CommentItems");
            }
        }

        private ObservableCollection<ClaimItem> _ClaimItems;
        public ObservableCollection<ClaimItem> ClaimItems
        {
            get
            {
                return _ClaimItems;
            }
            set
            {
                _ClaimItems = value;
                NotifyPropertyChanged("ClaimItems");
            }
        }

        private ReportItem _SelectedReport;
        public ReportItem SelectedReport
        {
            get
            {
                return _SelectedReport;
            }
            set
            {
                _SelectedReport = value;
                NotifyPropertyChanged("SelectedReport");
            }
        }

        public override void Load()
        {
            IsBusy = true;
            serviceComment.GetStreamComments(SelectedReport.nid);
            serviceClaim.GetClaim(SelectedReport.nid);

            TLMessage = String.Empty;
            TLVisibility = Visibility.Collapsed;
            CommentMessage = String.Empty;
            CommentVisibility = Visibility.Collapsed;
            isStatusDataMenunggu = true;
        }

        void CommentCompleted(object sender, LaporEventArgument e)
        {
            IsBusy = false;
            if (e.Value != null)
            {
                List<CommentItem> result = JsonConvert.DeserializeObject<List<CommentItem>>(e.Value);
                CommentItems = new ObservableCollection<CommentItem>(result);
              
            }
            if (CommentItems.Count == 0)
            {
                CommentMessage = "Belum ada komentar";
                CommentVisibility = Visibility.Visible;
            }
        }

        void ClaimCompleted(object sender, LaporEventArgument e)
        {
            IsBusy = false;
            if (e.Value != null)
            {
                List<ClaimItem> result = JsonConvert.DeserializeObject<List<ClaimItem>>(e.Value);
                ClaimItems = new ObservableCollection<ClaimItem>(result);
               
            }
            if (ClaimItems.Count == 0)
            {
                TLMessage = "Belum ada tindak lanjut";
                TLVisibility = Visibility.Visible;
            }
        }

        public void SendComment(string content) {
            isStatusDataMenunggu = false;
            IsBusy = true;
            StreamModel streamModel = new StreamModel();
            streamModel.reply_nid = SelectedReport.nid;
            streamModel.content = content;
            Point point =  SettingHelper.GetKeyValue<Point>("DefaultLocation");
            streamModel.latitude = point.X.ToString();
            streamModel.longitude = point.Y.ToString();
            ServiceHelper service = new ServiceHelper();
            service.SendStreamComment(streamModel);
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(SendCommentCompleted);
            UserModel userData = SettingHelper.GetKeyValue<UserModel>("USER_ACCOUNT");
            tempItem = new CommentItem();
            tempItem.first_name = userData.FirstName;
            tempItem.last_name = userData.LastName;
            tempItem.coordinate_lat = streamModel.latitude;
            tempItem.coordinate_lon = streamModel.longitude;
            tempItem.content = content;
            //CommentItems.Add(item);
        }

        private CommentItem tempItem;

        void SendCommentCompleted(object sender, LaporEventArgument e)
        {
            isStatusDataMenunggu = true;
            IsBusy = false;
            // Parse, Pasti masuk error
            if (e.Error != null)
            {
                JSonResultError result = JsonConvert.DeserializeObject<JSonResultError>(e.Error.Message);
                if (result.status == 1) // berhasil
                {
                    if (tempItem != null)
                    {
                        tempItem.nid = result.nid;
                        CommentItems.Add(tempItem);
                    }    
                }
                else {
                    MessageBox.Show(result.error);
                }
            }
        }

        public void Vote() {
            isStatusDataMenunggu = false;
            IsBusy = true;
            ServiceHelper service = new ServiceHelper();
            service.DataServiceCompleted += new EventHandler<LaporEventArgument>(VoteCompleted);
            service.StreamRate(SelectedReport.nid, 1);
        }

        void VoteCompleted(object sender, LaporEventArgument e)
        {
            isStatusDataMenunggu = true;
            IsBusy = false;
            
            JSonResultError result = JsonConvert.DeserializeObject<JSonResultError>(e.Value);
            if (result.status == 0)
            {
                MessageBox.Show("Anda sudah mendukung laporan ini");
            }
            else {
                AlreadyVote = true;
                MessageBox.Show("Anda mendukung laporan ini");
                int dukung = Int16.Parse(SelectedReport.agree);
                dukung = dukung + 1;
                SelectedReport.agree = dukung.ToString();
            }
        }
    }

}

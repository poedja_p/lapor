﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Device.Location;
using RadyaLabs.Common;

namespace Lapor.Helper
{
    public class LocationHelper
    {
        private GeoCoordinateWatcher watcher;
        public void GetLocation()
        {
            if (watcher == null)
            {
                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
                watcher.MovementThreshold = 20;
                watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(watcher_PositionChanged);
                watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
            }
            watcher.Start();
        }

        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            SettingHelper.SetKeyValue("DefaultLocation", new Point(e.Position.Location.Latitude, e.Position.Location.Longitude));
        }


        void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            Point point = new Point(0, 0);
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    if (watcher.Permission == GeoPositionPermission.Denied)
                    {
                        // Perlu?
                        //MessageBox.Show("you turn off real-time location service functionality, application will show data from your last position", "ATTENTION", MessageBoxButton.OK);
                    }

                    break;

                case GeoPositionStatus.Initializing:
                    break;

                case GeoPositionStatus.NoData:
                    break;

                case GeoPositionStatus.Ready:
                    SettingHelper.SetKeyValue("DefaultLocation", new Point(watcher.Position.Location.Latitude, watcher.Position.Location.Longitude));
                    break;
            }
        }
    }
}

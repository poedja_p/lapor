﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lapor.Helper
{
    public class MathHelper
    {
        public static DateTime ConvertFromEpoch(long value)
        { 
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.ToLocalTime().AddSeconds(value);
        //    return epoch;
        }

        public static long ConvertToEpoch(DateTime value) {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (value.ToFileTime() - epoch.ToLocalTime().ToFileTime()) / 10000000;
            //    return epoch;
        }
    }
}

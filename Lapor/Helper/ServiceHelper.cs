﻿using System;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Lapor.Model;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using RadyaLabs.Common;

namespace Lapor.Helper
{
    public class ServiceHelper
    {
        private Dictionary<string, object> ParametersRef;
        private string BoundaryString = "----------V2ymHFg03ehbqgZCaKO6jy";
        private string Token = "f8244f657702a1d9d3cdd7d43aa60bf8";
        public event EventHandler<LaporEventArgument> DataServiceCompleted;
        protected void OnDataServiceCompleted(LaporEventArgument e)
        {
            EventHandler<LaporEventArgument> handler = DataServiceCompleted;

            if (handler != null)
                handler(this, e);
        }

 
        private void ConstructRequest(Uri uri, string method, string postData, object userToken)
        {
            WebClient client = new WebClient();
            client.Headers["aplikasi"] = "lapor";
            client.Headers["Accept_Language"] = "en-US";
            client.Headers["KEYTOKEN"] = userToken.ToString();
            
            if (method == "POST")
            {
                client.UploadStringCompleted += client_UploadStringCompleted;
                client.Headers["Content-Type"] = "application/x-www-form-urlencoded";
                client.Encoding = Encoding.UTF8;
                client.UploadStringAsync(uri, method, postData, userToken);
            }
            else if (method == "GET")
            {
                client.DownloadStringCompleted += client_DownloadStringCompleted;
                client.DownloadStringAsync(uri, userToken);
            }
        }

        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                if (!e.Result.Contains("error"))
                    OnDataServiceCompleted(new LaporEventArgument(e.Result, e.UserState));
                else
                    OnDataServiceCompleted(new LaporEventArgument(null, new Exception(e.Result)));
            }
            catch (Exception ex)
            {
                OnDataServiceCompleted(new LaporEventArgument(null, ex));
            }
        }

        void client_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            try
            {
                //if (!e.Result.Contains("error"))
                    OnDataServiceCompleted(new LaporEventArgument(e.Result, e.UserState));
                //else
                //    OnDataServiceCompleted(new LaporEventArgument(null, new Exception(e.Result)));
            }
            catch (Exception ex)
            {
                if (ex is WebException)
                {
                    string s = new StreamReader((ex as WebException).Response.GetResponseStream()).ReadToEnd();
                }
                OnDataServiceCompleted(new LaporEventArgument(null, ex));
            }
        }

        private string ConstructQueryString(IDictionary<string, object> parameters)
        {
            string query = String.Empty;
            if (parameters != null)
            {
                int i = 0;
                foreach (var x in parameters)
                {
                    if (i == parameters.Count - 1)
                        query += String.Format("{0}={1}", x.Key, x.Value);
                    else
                        query += String.Format("{0}={1}&", x.Key, x.Value);
                    i++;
                }
            }
            // query += String.Format("&{0}={1}", "timestamp", DateTime.UtcNow.Ticks.ToString());

            return query;
        }

        private string GetHash(string plainText)
        {
            string processPlainText = (plainText);
            SHA1Managed s = new SHA1Managed();
            UTF8Encoding enc = new UTF8Encoding();

            s.ComputeHash(enc.GetBytes(processPlainText.ToCharArray()));
            System.Diagnostics.Debug.WriteLine("Original Text {0}, Access {1}", plainText, BitConverter.ToString(s.Hash).Replace("-", ""));
            return (BitConverter.ToString(s.Hash).Replace("-", "")).ToLowerInvariant();
        }

        private void ConstructRequestMultipart(Uri uri,object userToken)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(uri);

            myRequest.Method = "POST";
            myRequest.ContentType = string.Format("multipart/form-data; boundary={0}", BoundaryString);
            myRequest.Headers["aplikasi"] = "lapor";
            myRequest.Headers["Accept_Language"] = "en-US";
            myRequest.Headers["KEYTOKEN"] = userToken.ToString();

           
            myRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), myRequest);
        }

        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
            System.IO.Stream postStream = request.EndGetRequestStream(asynchronousResult);

            StreamWriter postDataWriter = new StreamWriter(postStream);

            foreach (var x in ParametersRef)
            {
                if (x.Value != null)
                {
                    if (x.Value.GetType() == typeof(String))
                    {
                        if (!string.IsNullOrEmpty((String)x.Value))
                        {
                            postDataWriter.Write("\r\n--" + BoundaryString + "\r\n");
                            postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", x.Key, x.Value);
                        }
                    }
                    else if (x.Value.GetType() == typeof(int))
                    {
                        postDataWriter.Write("\r\n--" + BoundaryString + "\r\n");
                        postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", x.Key, x.Value.ToString());
                    }
                    else if (x.Value.GetType() == typeof(double))
                    {
                        postDataWriter.Write("\r\n--" + BoundaryString + "\r\n");
                        postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", x.Key, x.Value.ToString());
                    }
                    else
                    {
                        if ((Stream)x.Value != null)
                        {
                            postDataWriter.Write("\r\n--" + BoundaryString + "\r\n");
                            postDataWriter.Write("Content-Disposition: form-data;"
                                                    + "name=\"{0}\";"
                                                    + "filename=\"{1}\""
                                                    + "\r\nContent-Type: {2}\r\n\r\n",
                                                    "file",
                                                   "photo" + ".jpg",
                                                    "image/jpeg");
                            postDataWriter.Flush();
                            byte[] buffer = new byte[1024];
                            int bytesRead = 0;

                            while ((bytesRead = ((Stream)x.Value).Read(buffer, 0, buffer.Length)) != 0)
                            {
                                postStream.Write(buffer, 0, bytesRead);
                            }
                            ((Stream)x.Value).Close();
                        }
                    }
                }
            }

            postDataWriter.Write("\r\n--" + BoundaryString + "--\r\n");
            postDataWriter.Flush();
            postStream.Close();

            request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                System.IO.Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);

                string result = streamRead.ReadToEnd();

                streamResponse.Close();
                streamRead.Close();
                response.Close();

                if (DataServiceCompleted != null)
                    DataServiceCompleted(this, new LaporEventArgument(result, null, "add_event"));
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                if (DataServiceCompleted != null)
                    DataServiceCompleted(this, new LaporEventArgument(null, ex));
            }

        }

        private string GetDateTimes(DateTime time)
        {
            string day = String.Empty;
            string month = String.Empty;
            if (time.Day < 10)
                day = "0" + time.Day;
            else
                day = time.Day.ToString();

            if (time.Month < 10)
                month = "0" + time.Month;
            else
                month = time.Month.ToString();

            string year = time.Year.ToString();

            return String.Format("{0}-{1}-{2}", year, month, day);
        }

     
        private string getUserToken() {
            return getUserData().KeyToken;
        }

        private UserModel getUserData() {
            UserModel userData = SettingHelper.GetKeyValue<UserModel>("USER_ACCOUNT");
            return userData;
        }

        public void Login(string email, string password) {
            Uri uri = new Uri(EndpointHelper.LOGIN);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("email", email);
            parameters.Add("password", password);
            string postData = ConstructQueryString(parameters);

            ConstructRequest(uri, "POST", postData, this.Token);
        }

        public void Register(string email, string password, string name1, string name2, string sex, string birthday, string phone) {
            Uri uri = new Uri(EndpointHelper.REGISTER);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("email", email);
            parameters.Add("password", password);
            parameters.Add("firstname", name1);
            parameters.Add("lastname", name2);
            parameters.Add("sex", sex);
            parameters.Add("birthday", birthday);
            parameters.Add("phone", phone);
            string postData = ConstructQueryString(parameters);

            ConstructRequest(uri, "POST", postData, this.Token);
        }

        public void Forgot(string email) {
            Uri uri = new Uri(EndpointHelper.FORGOT);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("email", email);
            string postData = ConstructQueryString(parameters);

            ConstructRequest(uri, "POST", postData, this.Token);
        }

        public void EditProfile(string name1, string name2, string phone, string sex, string birthday, string id, string isphoto) {
            Uri uri = new Uri(EndpointHelper.UPDATE_PROFILE);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("first_name", name1);
            parameters.Add("last_name", name2);
            parameters.Add("phone", phone);
            parameters.Add("sex", sex);
            parameters.Add("birthday", birthday);
            parameters.Add("no_id", id);
            string postData = ConstructQueryString(parameters);
            
            ConstructRequest(uri, "POST", postData, getUserToken());
        }

        public void GetProfile() {
            Uri uri = new Uri(EndpointHelper.USERPROFILE);
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetTopics() {
            Uri uri = new Uri(EndpointHelper.STREAMTOPICS);
            if (getUserData() != null)
            {
                ConstructRequest(uri, "GET", null, getUserToken());

            }
            // Parse : 
            // List<JsonResultTopic> obj = JsonConvert.DeserializeObject<List<JsonResultTopic>>(e.Value);
        }

        public void GetNotifications() {
            Uri uri = new Uri(EndpointHelper.NOTIFICATION + getUserData().UserId);
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetStreamsByTopic(string topic_nid)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAMS);
            uriBuiler.Append("/kat/");
            uriBuiler.Append(topic_nid);
            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetStreamsByTopic(string topic_nid,string tailNid)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAM_NID);
            uriBuiler.Append(tailNid);
            uriBuiler.Append("/kat/");
            uriBuiler.Append(topic_nid);
            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetStreamsByUsers(int user_id)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAMS);
            uriBuiler.Append("/user_id/");
            if (user_id == -1) uriBuiler.Append(getUserData().UserId);
            else uriBuiler.Append(user_id);
            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetStreamsByUsers(int user_id,string tailNid)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAM_NID + tailNid);
            uriBuiler.Append("/user_id/");
            if (user_id == -1) uriBuiler.Append(getUserData().UserId);
            else uriBuiler.Append(user_id);
            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }
       
        public void GetStreamsUserByCategory(string topic_nid, String opt=null)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAMS);
            uriBuiler.Append("/user_id/");
            uriBuiler.Append(getUserData().UserId);
            uriBuiler.Append("/kat/");
            uriBuiler.Append(topic_nid);
            if (opt != null) {
                uriBuiler.Append("/opt");
                uriBuiler.Append(opt);
            }
            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetStreamsUserByCategory(string tailNid, string topic_nid, String opt = null)
        {
            StringBuilder uriBuiler = new StringBuilder();
            uriBuiler.Append(EndpointHelper.STREAM_NID + tailNid);
            uriBuiler.Append("/user_id/");
            uriBuiler.Append(getUserData().UserId);
            uriBuiler.Append("/kat/");
            uriBuiler.Append(topic_nid);
            if (opt != null)
            {
                uriBuiler.Append("/opt");
                uriBuiler.Append(opt);
            }

            Uri uri = new Uri(uriBuiler.ToString());
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void Search(string text) {
            Uri uri = new Uri(EndpointHelper.SEARCH + text);
            ConstructRequest(uri, "GET", null, getUserToken());
            // Parse :
            // List<JsonResultLaporan> obj = JsonConvert.DeserializeObject<List<JsonResultLaporan>>(e.Value);
        }

        public void GetHotTopics() {
            Uri uri = new Uri(EndpointHelper.HOT_TOPICS);
            ConstructRequest(uri, "GET", null, getUserToken());
            // Parse :
            // List<JsonResultLaporan> obj = JsonConvert.DeserializeObject<List<JsonResultLaporan>>(e.Value);
        }

        public void GetAllReports()
        {
            Uri uri = new Uri(EndpointHelper.ALL_REPORTS);
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void GetAllReports(string tailNid)
        {
            Uri uri = new Uri(EndpointHelper.STREAM_NID + tailNid);
            ConstructRequest(uri, "GET", null, getUserToken());
            // Parse :
            // List<JsonResultLaporan> obj = JsonConvert.DeserializeObject<List<JsonResultLaporan>>(e.Value);
        }

        public void GetStreamComments(string nid) {
		    Uri uri = new Uri(EndpointHelper.STREAMCOMMENTS_NID + nid);
		    ConstructRequest(uri, "GET", null, getUserToken());
            // Parse :
            // List<Model.JsonResultComment> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Model.JsonResultComment>>(e.Value);
		
        }

        public void GetClaim(string nid) {
            Uri uri = new Uri(EndpointHelper.CLAIM_NID + nid);
            ConstructRequest(uri, "GET", null, getUserToken());
            // Parse :
            // List<Model.JsonResultClaim> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Model.JsonResultClaim>>(e.Value);
        }

        public void StreamPhoto(string hash, bool thumb) {
            Uri uri = new Uri(EndpointHelper.STREAMPHOTO + hash);
            if (thumb) uri = new Uri(EndpointHelper.STREAMPHOTO_THUMB + hash);
            ConstructRequest(uri, "GET", null, getUserToken());
        }

        public void StreamRate(string nid, int agree) {
           Uri uri = new Uri(EndpointHelper.STREAMRATE);
           Dictionary<string, object> parameters = new Dictionary<string, object>();
           parameters.Add("user_id", getUserData().UserId);
           parameters.Add("nid", nid);
           parameters.Add("agree",agree);
           string postData = ConstructQueryString(parameters);
           ConstructRequest(uri, "POST", postData, getUserToken());
        }

        public void StreamClaim(int nid) {
           Uri uri = new Uri(EndpointHelper.STREAMCLAIM);
           Dictionary<string, object> parameters = new Dictionary<string, object>();
           parameters.Add("user_id", getUserData().UserId);
           parameters.Add("nid", nid);
           string postData = ConstructQueryString(parameters);
           ConstructRequest(uri, "POST", postData, getUserToken());
        }

        public void GetVersion() { 
            //TODO : implement this
            // - no endpoint to implement this
        }

        public void SendStreamComment(StreamModel item) {
            Uri uri = new Uri(EndpointHelper.STREAMCOMMENT);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("user_id", getUserData().UserId);
            parameters.Add("content", item.content);
            parameters.Add("coordinate", item.coordinate);
            parameters.Add("latitude", item.latitude);
            parameters.Add("longitude", item.longitude);
            parameters.Add("photo", item.photo);
            parameters.Add("source", item.source);
            parameters.Add("address", item.address);
            parameters.Add("reply_nid", item.reply_nid);
            string postData = ConstructQueryString(parameters);
            ConstructRequest(uri, "POST", postData, getUserToken());
            // Parse : (msk error)
            // Status = 1 -> berhasil
        }

        public void SendStream(StreamModel item) {
            Uri uri = new Uri(EndpointHelper.STREAMREPORT);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("user_id", getUserData().UserId);
            parameters.Add("content", item.content);
            parameters.Add("coordinate", item.coordinate);
            parameters.Add("latitude", item.latitude);
            parameters.Add("longitude", item.longitude);
            parameters.Add("photo", item.photo);
            parameters.Add("source", item.source);
            parameters.Add("address", item.address);
            parameters.Add("topic_nid", item.topic_nid);
            if (item.SelectAnonim == 1)
                parameters.Add("is_anonim", 1);
            if (item.TypeReport == 2)
                parameters.Add("report_type", 2);
            string postData = ConstructQueryString(parameters);
            if (item.photo.Equals("0"))
                ConstructRequest(uri, "POST", postData, getUserToken());
            else
            {
                parameters.Remove("photo");
                parameters.Add("photo", "1");
                parameters.Add("photoStream", item.photoA);
                ParametersRef = parameters;
               
                ConstructRequestMultipart(uri,getUserToken());
            }

            // Parse : (msk error)
            // {"status":0,"error":"","nid":971438}
        }


        

    }
}

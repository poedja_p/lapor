﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Lapor.Helper
{
    public class EndpointHelper
    {
        public static string BASE_URL = "http://lapor.ukp.go.id/";
     
        public static string ENDPOINT = "index.php/api/lapor/";


        public static string CLAIM_NID = BASE_URL + ENDPOINT + "claims/nid/";

        public static string HOT_TOPICS = BASE_URL + ENDPOINT + "hottopiks";

        public static string ALL_REPORTS = BASE_URL + ENDPOINT + "streams/";

        public static string LOGIN = BASE_URL + ENDPOINT + "verify";

        public static string NOTIFICATION = BASE_URL + ENDPOINT + "notifikasiku/id/";

        public static string REGISTER = BASE_URL + ENDPOINT + "register";

        public static string USERPROFILE = BASE_URL + ENDPOINT + "userprofile";

        public static string UPDATE_PROFILE = "http://lapor.ukp.go.id/api/lapor/userprofile";

        public static string FORGOT = BASE_URL + ENDPOINT + "forgot_password";

        public static string SEARCH = BASE_URL + ENDPOINT + "cari/cari/";

        public static string STREAMCOMMENT = BASE_URL + ENDPOINT + "streamcomment";

        public static string STREAMCOMMENTS_NID = BASE_URL + ENDPOINT + "streamcomments/nid/";

        public static string STREAMCLAIM = BASE_URL + ENDPOINT + "sanggah";

        public static string STREAMPHOTO = BASE_URL + "images/stream/";

        public static string STREAMPHOTO_THUMB = BASE_URL + "images/stream/thumb/thumb_";

        public static string STREAMRATE = BASE_URL + ENDPOINT + "streamrate";

        public static string STREAMREPORT = BASE_URL + ENDPOINT + "stream";

        public static string STREAMTOPICS = BASE_URL + ENDPOINT + "streamtopics";

        public static string STREAM_NID = BASE_URL + ENDPOINT + "streams/nid/";

        public static string STREAMS = BASE_URL + ENDPOINT + "streams";

        
        //INSERT ANY URL ENDPOINT API HERE, Alphabetically


    }
}

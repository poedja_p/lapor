﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lapor.Helper
{
    public class MessageHelper
    {
        public static string LOADING = "Menghubungi server..";
        public static string SEARCHING = "Melakukan pencarian..";
        public static string FETCH = "Menunggu data..";
        public static string SEND_DATA = "Mengirim data..";

        public static string REQUEST_FAILED = "Permintaan Anda gagal. Mohon dicoba kembali dalam beberapa saat";

        public static string CONFIRMATION = "Konfirmasi";
        public static string CONGRATULATION = "Selamat";
        public static string SORRY = "Kesalahan";
    }
}

﻿#pragma checksum "E:\Work\RadLabs\2012-2013\Lapor\Lapor\Lapor\View\HotReportPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EE7EBD921A0F84E51D6B21B38708ABD8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using RadyaLabs.Control;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Lapor.View {
    
    
    public partial class HotReportPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.ListBox HotReportListBox;
        
        internal RadyaLabs.Control.CustomListBox AllReportListBox;
        
        internal RadyaLabs.Control.CustomListBox MyReportListBox;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Lapor;component/View/HotReportPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.HotReportListBox = ((System.Windows.Controls.ListBox)(this.FindName("HotReportListBox")));
            this.AllReportListBox = ((RadyaLabs.Control.CustomListBox)(this.FindName("AllReportListBox")));
            this.MyReportListBox = ((RadyaLabs.Control.CustomListBox)(this.FindName("MyReportListBox")));
        }
    }
}

